const float color1[7][3] = { // identifyZone()用の固定値
  //{6,14,6},{60,76,59},{60,20,21},{10,16,42},{2,6,25},{48,7,3}//1,2,3,4,5,赤
  {
    6, 14, 6                              }
  , {
    60, 80, 59                              }
  , {
    65, 20, 21                              }
  , {
    10, 16, 50                              }
  , {
    2, 6, 25                              }
  , {
    80, 80, 80                              }
    ,
    {100,100,100}//1,2,3,4,5,灰色
};

int goblack() {
  static int count = 0;
  motorL_G = SPEED + 10;
  motorR_G = SPEED + 10;

  if (red_G < 15 && green_G < 15 && blue_G < 15)
    count++;
  else
    count = 0;

  if (count > 3)
    return 1;
  else
    return 0;

}
int gowhite() {
  static int count = 0;
  motorL_G = SPEED + 10;
  motorR_G = SPEED + 10;
  if (red_G > 75 && green_G > 75 && blue_G > 75)
    count++;
  else
    count = 0;
  if (count > 3)
    return 1;
  else
    return 0;
}



/*****赤をライントレースしながら黒認識******/
int redlinetracePID()
{
  static unsigned long timePrev = 0;
  static float lightPrevPD = 0.0;
  static int count = 0;

  float lightNowPD;
  float error, errorSP;
  float diff, diffSP;
  float speedDiff;

  float target = 50;
  float Kp = 2.1;//大きくすれば誤差を修正するためのロボットの操作量(左右の旋回)が大きくなり,急カーブを曲がれるようになるが,大きくしすぎると蛇行を繰り返す
  float Kd = 1.5;//大きくすると急激な誤差の変化に抗する方向の制御が加わり蛇行(振動)が抑制,大きくしすぎると副作用もある

  lightNowPD =  (green_G + blue_G ) / 2.0;
  error = lightNowPD - target;
  errorSP = map(error, -target, target, -SPEED, SPEED );
  diff = (lightNowPD - lightPrevPD) / (timeNow_G - timePrev );
  diffSP  = map(diff, -100.0, 100.0, -SPEED, SPEED );

  speedDiff = Kp * errorSP + Kd * diffSP;

  if (dir_G == 0) {
    if (speedDiff > 0) {
      motorL_G = SPEED - speedDiff;
      motorR_G = SPEED;
    }
    else {
      motorL_G = SPEED;
      motorR_G = SPEED + speedDiff;
    }
    //Serial.println("left");
  }
  else{
    if (speedDiff > 0) {
      motorL_G = SPEED;
      motorR_G = SPEED - speedDiff;
    }
    else {
      motorL_G = SPEED + speedDiff;
      motorR_G = SPEED;
    }
    //Serial.println("right");
  }



  timePrev = timeNow_G;
  lightPrevPD = lightNowPD;

  /**水色を検知**/
  if (red_G > 20 && red_G < 40 && green_G > 50 && green_G < 70 && blue_G > 60 && blue_G < 80){
    //if (red_G < 15 && green_G < 15 && blue_G < 15)
    //buzzer.play(">g32>>c32");
    direct_G = 0;
    count++;
  }
  else
    count = 0;
  if (count > 3)
    return 1;
  else
    return 0;
}

/******赤を認識するまで直進する******/
int gored()
{
  static int count = 0;
  motorL_G = SPEED;
  motorR_G = SPEED;

  if (red_G > 40 && green_G < 15 && blue_G < 15)
    count++;
  else
    count = 0;

  if (count > 2){
    count = 0;
    return 1;
  }
  else
    return 0;
}

void identifyjct()
{
  // float minDistance; グローバル変数で定義
  motorL_G = SPEED;
  motorR_G = SPEED;
  static float distance = 999999;

  for ( int i = 0; i < 7; ++i ) {
    distance = (red_G - color1[i][0]) * (red_G - color1[i][0])
      + (green_G - color1[i][1]) * (green_G - color1[i][1])
        + (blue_G - color1[i][2]) * (blue_G - color1[i][2]);
    if ( distance < minDistance) {
      minDistance = distance;
      jctnumber_G = i;
      //Serial.println("color=");
      //Serial.println(i);
      buzzer.play(">g32>>c32");
    }
  }
}

int identifyroot(){
  // float minDistance; グローバル変数で定義
  motorL_G = SPEED;
  motorR_G = SPEED;
  static int countbrown = 0; // この関数が初めて呼ばれた時にのみ初期化される
  static int countblue = 0; // この関数が初めて呼ばれた時にのみ初期化される
  static int countwhite = 0; // この関数が初めて呼ばれた時にのみ初期化される

  /* if(red_G < 20 && green_G < 10 && blue_G < 7)//茶
   countbrown++;
   else
   countbrown = 0;*/
  if (red_G < 20 && green_G < 10 && blue_G < 5) //茶
    countbrown++;
  else
    countbrown = 0;


  /*if(red_G < 23 && green_G > 28 && blue_G > 46)//青
   countblue++;
   else
   countblue = 0;*/
  if (red_G < 20 && red_G > 10 && green_G < 45 && green_G > 30 && blue_G > 65) //日笠家青
    countblue++;
  else
    countblue = 0;

  if (red_G > 75 && green_G > 75 && blue_G > 75) //白
    countwhite++;
  else
    countwhite = 0;

  if (countwhite > 3 || countblue > 3 || countbrown > 3 ) {
    if (countwhite > 3 && direct_G == 0) {
      ssss_G = 0;
      countwhite = 0;
    }
    else if (countblue > 3) {
      //buzzer.play(">g32>>c32");
      direct_G = 1;
      ssss_G = 1;
      countblue = 0;
    }
    else if (countbrown > 3 && direct_G == 0) {
      ssss_G = 2;
      countbrown = 0;
    }
    //Serial.println("dir=");
    //Serial.println(ssss_G);
    return 1;

  }
  else
    return 0;

}

void zone()
{

  int zoneNumber;
  int done = 0;

  switch ( mode_G ) {
  case 0: // setupが必要ならここ（必要が無くても形式的に）
    mode_G = 1;
    ssss_G = 0;
    dir_G = 1;
    break;
  case 1: // 赤を検知するまで直進
    done = gored();
    if ( done == 1 ) {
      motors.setSpeeds(0, 0);
      buzzer.play(">g32>>c32");

      mode_G = 2;
    }
    break;
  case 2: // ライントレース（黒を検知するまで）
    done = redlinetracePID();
    if ( done == 1 ) {
      mode_G = 3;
      minDistance = 999999;
    }
    break;
  case 3://点数,分かれ目認識
    goStraight();  
    identifyjct();
    done = identifyroot();
    if (jctnumber_G == 5) {
      buzzer.play(">g32>>c32");
      mode_G = 5;
    }
    else if (done == 1) {
      switch(ssss_G){
      case 0:
        dir_G = 0;//左
        mode_G =  4;//白
        break;
      case 1:
        dir_G = 1;
        mode_G = 4;
        break;
      case 2:
        dir_G = 0;
        mode_G = 4;
      }
    }
    break;
  case 4://白を検知するまで直進
    goStraight();
    done = identifyColor( 1 );

    if ( done == 1 ) {
      mode_G = 2;
    }
    break;
  case 5:// 黒を検知するまで直進
    goStraight();
    done = identifyColor( 0 );
    if ( done == 1 )
      mode_G = 6;
    break;
  case 6: // ライントレース（黄と黒の混合色を検知するまで）
    linetracePID();
    done = identifyColor( 2 );
    if ( done == 1 ) {
      mode_G = 7;
    }
    break;
  case 7: // 白を検知するまで直進
    goStraight();
    done = identifyColor( 1 );
    if ( done == 1 ) {
      mode_G = 8;
    }
    break;
  case 8: // ライントレース（黄と黒の混合色を検知するまで）
    linetracePID();
    done = identifyColor( 2 );
    if ( done == 1 ) {
      mode_G = 9;
    }
    break;
  case 9: // identifyZone()のsetup
    minDistance = 9999999;
    mode_G = 10;
    break;
  case 10: // 白を検知するまで直進（その間ゾーン番号を検知）
    goStraight();
    zoneNumber = identifyZone();
    done = identifyColor( 1 );
    if ( done == 1 ) {
      zoneNumber_G = zoneNumber;
      mode_G = 0;
    }
    break;


  default:
    break;
  }
}


int steadyState( unsigned long period )
{
  static int flagStart = 0;
  static  unsigned long startTime = 0;

  if ( flagStart == 0 ) {
    startTime = timeNow_G;
    flagStart = 1;
  }

  if ( timeNow_G - startTime > period ) {
    flagStart = 0;
    startTime = 0; // 必要ないがstatic変数は形式的に初期化
    return 1;
  }
  else
    return 0;
}




