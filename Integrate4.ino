#include <Wire.h>
#include <ZumoMotors.h>
#include <Pushbutton.h>
#include <ZumoBuzzer.h>
#include <LSM303.h>

ZumoMotors motors;
ZumoBuzzer buzzer;
Pushbutton button(ZUMO_BUTTON);
LSM303 compass;

#define SPEED          120 // default motor speed 

float red_G, green_G, blue_G; //  RGB values
int zoneNumber_G; // zone number
int jctnumber_G;//ジャンクション番号を表す状態変数
int mode_G; // mode in each zone
unsigned long timeInit_G, timeInitZone_G, timeNow_G, timeNowZone_G; // start time, current time, 
int motorR_G, motorL_G;  // input values to the motors
int ssss_G = 0;//三番目の色何を認識したか
int dir_G = 0;//どの方向に行くか格納
int direct_G = 0;
void setup()
{
  Serial.begin(9600);
  Wire.begin();

  button.waitForButton();

  setupColorSensor(); // カラーセンサーのsetup
  CalibrationColorSensor(); // カラーセンサーのキャリブレーション
  //setupCompass(); // 地磁気センサーのsetup
  //CalibrationCompass(); // 地磁気センサーのキャリブレーション
  buzzer.play(">g32>>c32");

  zoneNumber_G = 0;
  mode_G = 0;
  timeInit_G = millis();
  timeNowZone_G = 0;

  button.waitForButton();
}

void loop()
{
  readRGB();
  clearInterrupt();
  timeNow_G = millis() - timeInit_G;
  motors.setSpeeds(motorL_G, motorR_G);
  sendData();

  switch ( zoneNumber_G ) {
  case 0:
    startToZone(); // start to zone
    break;
  case 1:
    zone(); // zone 1
    break;
  case 2:
    zone(); // zone 1
    break;
  case 3:
    zone(); // zone 1
    break;
  case 4:
    zone(); // zone 1
    break;
  case 5:
    zone(); // zone 1
    break;
  case 6:
    zone(); // zone 1
    break;
  case 7:
    zone(); // zone 1
    break;
  case 8:
    zoneToZone(); // zone to zone
    break;
  default:
    break;
  }
  
}
void sendData()
{
  static int inByte = 0;
  if( Serial.available() > 0 || inByte == 0 ){ // 最初の一回はデータ送信要求なしでもデータ送信.それ以降はデータ送信要求を受信した時のみデータ送信.
    inByte = Serial.read();
    inByte = 1;
    Serial.write('H'); // データのヘッダー
    Serial.write(zoneNumber_G);
    Serial.write(mode_G);
    Serial.write(motorL_G);
    Serial.write(motorR_G);
    Serial.write((int)(timeNow_G/1000)); // 整数化した時の値が-32768∼32767 の範囲
    Serial.write((int)(timeNowZone_G/1000)); //した時の値が-32768∼327
    Serial.write((int)red_G);
    Serial.write((int)green_G);
    Serial.write((int)blue_G);
    /*
Serial.write(a_G);
     // 値の範囲が 0∼255 の場合
     Serial.write(b_G >> 8); // 値の範囲が 0∼65535 の場合
     Serial.write(b_G & 255);
     Serial.write(c_G >> 8); // 値の範囲が-32768∼32767 の場合
     Serial.write(c_G & 255);
     Serial.write((int)(100*f_G) >> 8); // 100 倍して整数化(小数点 2 位まで送信)
     Serial.write((int)(100*f_G) & 255); // 整数化した時の値が-32768∼32767 の範囲
     */
  }
}



